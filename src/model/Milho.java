/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Eduarda
 */
public class Milho extends Pizza {
    private final Ingrediente i = new Ingrediente("milho", 1);
    
    @Override
    public String getReceita(){
        return super.getReceita() + i.toString() + "Colocar o milho depois whatever.";
    }
}
