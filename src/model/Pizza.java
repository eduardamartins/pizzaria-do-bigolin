/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Eduarda
 */
public class Pizza {

    private String nomeCliente, nomePizza, receita;
    private int pizzaID;
    private boolean cozida = false;
    private ArrayList<Ingrediente> listaIngredientes;
    private final Ingrediente molhoTomate = new Ingrediente("Colheres de Molho de Tomate", 3);
    private final Ingrediente queijoVegano = new Ingrediente("Fatias de Queijo Vegano", 6);
    
    Pizza(){
        receita = molhoTomate.toString() + queijoVegano.toString();
    }

    public void update() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String sql = "update pizza set pronta = 1 where id = ?";
 
        try  {
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, getPizzaID());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
    }
    
    public boolean insert() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "INSERT INTO pizza(id, nome, cliente, pronta, receita) VALUES(pizza_pedido.nextval,?,?,?,?)";
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.nomePizza);
            preparedStatement.setString(2, this.nomeCliente);
            preparedStatement.setBoolean(3, this.cozida);
            preparedStatement.setString(4, this.receita);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
        return true;
    }

    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        String insertTableSQL = " DELETE FROM pizza WHERE id = ?";
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, getPizzaID());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
        return true;
    }

    public void load() {
        String selectSQL = "SELECT * FROM pizza WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.pizzaID);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.setNomePizza(rs.getString("nome"));
                this.setNomeCliente(rs.getString("cliente"));
                this.setCozida(rs.getBoolean("pronta"));
                this.setReceita(rs.getString("receita"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    public static List<Pizza> getNaoProntos() {
        String selectSQL = "SELECT * FROM pizza WHERE pronta = 0";
        ArrayList<Pizza> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Pizza p = new Pizza();
                p.setPizzaID(rs.getInt("id"));
                p.setNomePizza(rs.getString("nome"));
                p.setNomeCliente(rs.getString("cliente"));
                p.setCozida(rs.getBoolean("pronta"));
                p.setReceita(rs.getString("receita"));
                lista.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return lista;
    }

    public static List<Pizza> getAll() {
        String selectSQL = "SELECT * FROM pizza";
        ArrayList<Pizza> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Pizza p = new Pizza();
                p.setPizzaID(rs.getInt("id"));
                p.setNomePizza(rs.getString("nome"));
                p.setNomeCliente(rs.getString("cliente"));
                p.setCozida(rs.getBoolean("pronta"));
                p.setReceita(rs.getString("receita"));
                lista.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return lista;
    }
    
    public boolean cozinhaPizza() {
        return true;
    }

    public int getPizzaID() {
        return pizzaID;
    }

    public void setPizzaID(int pizzaID) {
        this.pizzaID = pizzaID;
    }

    public String getNomePizza() {
        return nomePizza;
    }

    public void setNomePizza(String nomePizza) {
        this.nomePizza = nomePizza;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String s) {
        this.nomeCliente = s;
    }

    public boolean isCozida() {
        return cozida;
    }

    public void setCozida(boolean cozida) {
        this.cozida = cozida;
    }

    public String getReceita() {
        return receita;
    }

    public void setReceita(String receita) {
        this.receita = receita;
    }

    public void addIngrediente(Ingrediente i) {
        receita = receita + i.toString();
    }
    
    public String getStatus(){
        String status = "Pendente";
        if(this.cozida==true){
            status = "Pronta";
        }
        return status;
    }

}
