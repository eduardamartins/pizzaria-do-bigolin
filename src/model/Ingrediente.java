/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Eduarda
 */
public class Ingrediente {
    private String nome;
    private int quantidade;
    
    Ingrediente(String nome, int quantidade){
        this.nome = nome;
        this.quantidade = quantidade;
    }
    
    @Override
    public String toString(){
        return quantidade + " " + nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
}
