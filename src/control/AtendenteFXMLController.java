/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.Calabresa;
import model.Conexao;
import model.Milho;
import model.Mussarela;
import model.Pizza;

/**
 * FXML Controller class
 *
 * @author Eduarda
 */
public class AtendenteFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField nomeClienteTF;
    @FXML
    private TableView<Pizza> aTabela;
    @FXML
    private TableColumn<?, ?> aPizzaIDCol, aClienteCol, aPizzaCol, aProntaCol;
    @FXML
    private Label mAviso;
    private Pizza pizza = null;

    @FXML
    private void enviar(){
        if(!pizza.isCozida()){
            mAviso.setText("Impossível. Pedido incompleto.");
        }
        else{
            pizza.delete();
            aTabela.getItems().remove(pizza);
            aTabela.refresh();
            mAviso.setText("Pedido enviado para entrega.");
        }
    }
    
    @FXML
    private void voltar() {
        Main.trocaTela("TelaInicialFXML.fxml");
    }

    @FXML
    private void cancelar() {
        if (!pizza.isCozida()) {
            pizza.delete();
            aTabela.getItems().remove(pizza);
            aTabela.refresh();
            mAviso.setText("Pedido removido");
        } else {
            mAviso.setText("Impossível. Pedido pronto.");
        }
    }

    private void makeColumns() {
        aPizzaIDCol.setCellValueFactory(new PropertyValueFactory<>("pizzaID"));//precisa ter um getNome    
        aClienteCol.setCellValueFactory(new PropertyValueFactory<>("nomeCliente"));//precisa ter um getNome    
        aPizzaCol.setCellValueFactory(new PropertyValueFactory<>("nomePizza"));
        aProntaCol.setCellValueFactory(new PropertyValueFactory<>("status"));
    }

    @FXML
    private void selecionar(MouseEvent event) {
        pizza = aTabela.getSelectionModel().getSelectedItem();//Pega quem está selecionado na tabela
    }

    private void addItems() {
        aTabela.getItems().clear();
        for (Pizza p2 : Pizza.getAll()) {
            aTabela.getItems().add(p2);
        }
    }

    @FXML
    public void calabresa() {
        Calabresa c = new Calabresa();
        c.setNomeCliente(nomeClienteTF.getText());
        c.setNomePizza("Calabresa");
        c.setReceita(c.getReceita());
        c.setCozida(false);
        c.insert();
        Pizza.getAll();
        this.addItems();
    }

    @FXML
    public void mussarela() {
        Mussarela m = new Mussarela();
        m.setNomeCliente(nomeClienteTF.getText());
        m.setNomePizza("Mussarela");
        m.setReceita(m.getReceita());
        m.setCozida(false);
        m.insert();
        Pizza.getAll();
        this.addItems();
    }

    @FXML
    public void milho() {
        Milho m = new Milho();
        m.setNomeCliente(nomeClienteTF.getText());
        m.setNomePizza("Milho");
        m.setReceita(m.getReceita());
        m.setCozida(false);
        m.insert();
        Pizza.getAll();
        this.addItems();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.makeColumns();
        this.addItems();
        aTabela.refresh();
    }

}
