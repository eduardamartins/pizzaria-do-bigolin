/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 *
 * @author Eduarda
 */
public class TelaInicialFXMLController implements Initializable {
    
    @FXML
    public void cozinhar(){
        Main.trocaTela("CozinheiroFXML.fxml");
    }
    @FXML
    public void atender(){
        Main.trocaTela("AtendenteFXML.fxml");
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
