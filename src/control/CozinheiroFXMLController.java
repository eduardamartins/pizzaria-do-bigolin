/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.Conexao;
import model.Pizza;

/**
 * FXML Controller class
 *
 * @author Eduarda
 */
public class CozinheiroFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TableView<Pizza> cTabela;
    @FXML
    private TableColumn<?, ?> cPizzaIDCol, cNomePizzaCol, cReceitaCol;
    
    private Conexao c = new Conexao();
    private Connection connection = c.getConexao();
    private Pizza pizza;
    
    @FXML
    private void voltar(){
        Main.trocaTela("TelaInicialFXML.fxml");
    }
    
    private void makeColumns(){
        cPizzaIDCol.setCellValueFactory(new PropertyValueFactory<>("pizzaID"));//precisa ter um getNome    
        cNomePizzaCol.setCellValueFactory(new PropertyValueFactory<>("nomeCliente"));//precisa ter um getNome    
        cReceitaCol.setCellValueFactory(new PropertyValueFactory<>("receita"));
    }
    
    @FXML
    private void selecionar(MouseEvent event) {
        pizza = cTabela.getSelectionModel().getSelectedItem();//Pega quem está selecionado na tabela
    }
    
    private void addItems(){
        cTabela.getItems().clear();
        for(Pizza p2 : Pizza.getNaoProntos())
        {
            cTabela.getItems().add(p2);
        }
    }
    
    @FXML
    public void cozinhar(){
        pizza.load();
        pizza.setCozida(true);
        pizza.update();
        Pizza.getNaoProntos();
        cTabela.getItems().remove(pizza);
        cTabela.refresh();
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.makeColumns();
        this.addItems();
    }    
    
}
